package servidor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;



public class AdminBBDD_servidor {

	public String JDBC_DRIVER;
	public String DB_URL;
	public String USER;
	public String PASS;

	public static Connection conn = null;

	public Statement stmt = null;


	public AdminBBDD_servidor(String JDBC_DRIVER, String DB_URL, String USER, String PASS) {
		this.JDBC_DRIVER = JDBC_DRIVER;
		this.DB_URL = DB_URL;
		this.USER = USER;
		this.PASS = PASS;

		Conexion();

	}

	public void Conexion() {

		try {
			Class.forName("com.mysql.jdbc.Driver");

			System.out.println("CONECTANDO A BASE DE DATOS...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			System.out.println("CONEXION EXITOSA");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("NO SE HA PODIDO CONECTAR A LA BBDD " + e);
		}

	}
	
	public void Login(String usr, String pass) {
		
		String usuario;
		String contr;
		try {
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT id, usuario, password FROM usuarios";
			ResultSet rs = stmt.executeQuery(sql);
			
			usuario = usr;
			contr = pass;

			while (rs.next()) {
				
				if (rs.getString("usuario").equals(usuario)&& rs.getString("password").equals(contr)) {
					JOptionPane.showMessageDialog(null,
						    "HA INGRESADO CORRECTAMENTE",
						    "LOGIN CORRECTO", JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null,
							"BIENVENIDO Sr.Sra:  " + usuario);
				}else {
					
				}
				
				rs.close();
				stmt.close();
				conn.close();

			}

		} catch (Exception e) {
			System.out.println("NO SE HA PODIDO GENERAR LA CONSULTA");
		}

	}

	

}
