package servidor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Properties;

public class Main_Servidor implements InterfaceServidor {

	static AdminBBDD_servidor ad = null;

	/*
	 * public Main_Servidor() throws RemoteException {
	 * 
	 * super();
	 * 
	 * }
	 */

	// ------------------------------------------------------------------------------------//

	private class InfoUsuarios {

		private String id_cliente;
		private String usuario;
		private String pass;

		public InfoUsuarios(String id_cliente, String usuario, String pass) {
			this.id_cliente = id_cliente;
			this.usuario = usuario;
			this.pass = pass;
		}

		public String getId_cliente() {
			return id_cliente;
		}

		public void setId_cliente(String id_cliente) {
			this.id_cliente = id_cliente;
		}

		public String getUsuario() {
			return usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		public String getPass() {
			return pass;
		}

		public void setContrase�a(String pass) {
			this.pass = pass;
		}

	}

	// ------------------------------------------------------------------------------------//

	private HashMap<String, InfoUsuarios> tabla_clientes = new HashMap<String, InfoUsuarios>();
	private int tiempo_paso_espera = 1;

	@Override
	public void ExtraerContactos(String id_cliente) throws RemoteException {
		InfoUsuarios info = tabla_clientes.get(id_cliente);

	}
	
	@Override
	public void Logear(String user, String pass) throws RemoteException {
		ad.Login(user, pass);
		
	}


	// ------------------------------------------------------------------------------------//

	public static void main(String[] args) throws RemoteException {

		String bbdd = "";
		String usuario = "";
		String contrase�a = "";

		Properties propiedades = new Properties();
		InputStream entrada = null;
		try {
			File miFichero = new File("src/configuracion.ini");
			if (miFichero.exists()) {
				entrada = new FileInputStream(miFichero);
				// cargamos el archivo de propiedades
				propiedades.load(entrada);
				// obtenemos las propiedades y las imprimimos
				bbdd = propiedades.getProperty("basedatos");
				usuario = propiedades.getProperty("usuario");
				contrase�a = propiedades.getProperty("clave");
				System.out.println("FICHERO .INI CORRECTO");
			} else
				System.err.println("Fichero .INI no encontrado");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		ad = new AdminBBDD_servidor("com.mysql.jdbc.Driver",
				"jdbc:mysql://localhost/" + bbdd, usuario, contrase�a);
		ad.Conexion();

		// --------------------------------------------------------------------------------//
		// ------------------------------------------------------------------------------//

		Registry reg = null;
		try {
			System.out
					.println("Crea el registro de objetos, escuchando en el puerto 555");
			reg = LocateRegistry.createRegistry(5555);
		} catch (Exception e) {
			System.out.println("ERROR: No se ha podido crear el registro");
			e.printStackTrace();
		}
		System.out.println("Creando el objeto servidor");
		Main_Servidor serverObject = new Main_Servidor();
		try {
			System.out
					.println("Inscribiendo el objeto servidor en el registro");
			System.out.println("Se le da un nombre �nico: Agenda");
			reg.rebind("Agenda", (InterfaceServidor) UnicastRemoteObject
					.exportObject(serverObject, 0));
		} catch (Exception e) {
			System.out
					.println("ERROR: No se ha podido inscribir el objeto servidor.");
			e.printStackTrace();
		}
	}

	

}
