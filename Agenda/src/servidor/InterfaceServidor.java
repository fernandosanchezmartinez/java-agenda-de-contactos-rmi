package servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceServidor extends Remote {

	/*public void registro(String id_cliente, String usuario, String contraseņa)
			throws RemoteException;*/
	
	public void ExtraerContactos(String id_cliente) throws RemoteException;
	public void Logear(String user, String pass) throws RemoteException;

}
