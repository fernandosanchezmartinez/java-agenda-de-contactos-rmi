package cliente;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import servidor.InterfaceServidor;

public class Main_Cliente {

	static VistaLogin vl;

	public static void main(String[] args) throws RemoteException {

		InterfaceServidor agenda = null;
		try {
			System.out.println("Localizando el registro de objetos remitos");
			Registry registry = LocateRegistry.getRegistry("localhost", 5555);
			System.out.println("Obteniendo el stab del objeto remoto");
			agenda = (InterfaceServidor) registry.lookup("Agenda");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (agenda != null) {
			System.out.println("Realizando operaciones con el objeto remoto");
			vl = new VistaLogin();
			vl.frame.setVisible(true);
		}
	}
}
